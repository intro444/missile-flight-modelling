import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation


''' Dawid Grzeszczak 175821
        Zadanie nr. 2
            MMM
'''

fig = plt.figure()
ax = plt.axes(xlim=(-2, 20), ylim=(-100, 300))
line, = ax.plot([], [], lw=2)

t0 = 0.0
h = 0.01
g = 9.8
tn = 17

x1 = []
y1 = []

m = float(input("Podaj m: "))
C = float(input("Podaj C: "))
Vo = float(input("Podaj Vo: "))
#tn = float(input("Podaj czas wykresu: "))
#Ho = float(input("Podaj Wysokość początkową: "))


def F(t, y):
    F = np.zeros(2)
    F[0] = y[1]
    F[1] = -g - (C / m) * (y[1]) * abs((y[1]))
    return F


def runkutta(F, t, y, h):
    K0 = h * F(t, y)
    K1 = h * F(t + h / 2.0, y + K0 / 2.0)
    K2 = h * F(t + h / 2.0, y + K1 / 2.0)
    K3 = h * F(t + h, y + K2)
    return (K0 + 2.0 * K1 + 2.0 * K2 + K3) / 6.0


def rozniczkowanie(F, t, y, tn, h):
    T = []
    Y = []
    T.append(t)
    Y.append(y)
    while t < tn:
        h = min(h, tn - t)
        y = y + runkutta(F, t, y, h)
        t = t + h
        T.append(t), Y.append(y)
    return np.array(T), np.array(Y)


def maxWartosc(T, Y):
    Yn = Y[:, 0]
    xm = 0
    ym = np.amax(Yn)
    for i in range(len(Yn)):
        if ym == Yn[i]:
            xm = T[i]
    return xm, ym


def czasLotu(T, Y):
    Y1 = Y[:, 0]
    Yn = Y1[len(Y1) // 2:]
    yt = min(Yn, key=abs)
    for i in range(len(Y1)):
        if yt == Y1[i]:
            xm = T[i]
    return xm, yt


y = np.array([0, Vo])
T, Y = rozniczkowanie(F, t0, y, tn, h)
H = Y[:, 0]
print(H)

def Rysowanie():
    xt, yt = czasLotu(T, Y)
    tp, yp = maxWartosc(T, Y)
    print("Najwyższy punkt: " + str(yp))
    print("Czas lotu : " + str(xt))
    plt.scatter(tp, yp, color="RED")
    plt.scatter(xt, yt, color="GREEN")
    plt.grid(True)



def init():
    line.set_data([], [])
    return line,


def animacja(frame):
    x1.append(T[frame])
    y1.append(H[frame])
    line.set_data(x1[:-1], y1[:-1])
    return line,

Rysowanie()
anim = animation.FuncAnimation(fig, animacja, init_func=init, frames=len(T), interval=5, blit=True, repeat=False)

plt.show()